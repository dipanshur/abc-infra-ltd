
terraform {
    backend "s3" {
        bucket = "terraform-bucket-state-abc"
        key = "tfstate/terraform.tfstate"
        region = "us-east-1"
        dynamodb_table = "terraform-state"
        profile = "default"
    }

}
