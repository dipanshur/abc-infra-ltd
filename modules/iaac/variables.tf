variable "vpc_cidr" {
    description = "This variables takes CIDR input"
    default = "10.0.0.0/16"
}

variable "vpc_subnet_cidr" {
    description = "This var takes cidr input"
    default = ""
}

variable "vpc_subnet_az" {
    default = ""
}