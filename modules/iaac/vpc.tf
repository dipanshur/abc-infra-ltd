
## VPC ##
resource "aws_vpc" "abc_vpc" {
    cidr_block = var.vpc_cidr
}

## Subnet ##

resource "aws_subnet" "abc_vpc_subnet" {
    vpc_id = aws_vpc.abc_vpc.id
    cidr_block = var.vpc_subnet_cidr
    availability_zone = var.vpc_subnet_az
}