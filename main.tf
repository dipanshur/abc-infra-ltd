provider "aws" {
    region = "us-east-1"
}

module "abc-infra-setup" {
    source = "./modules/iaac"

    vpc_cidr = "172.31.0.0/24"
    vpc_subnet_cidr = "172.31.0.0/28"
    vpc_subnet_az = "us-east-1a"
}

module "xyz_infra_setup" {
    source = "./modules/iaac"

    vpc_cidr = "192.168.0.0/24"
    vpc_subnet_cidr = "192.168.1.0/28"
    vpc_subnet_az = "us-east-1a"
}